/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package StringProcessing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author sverma
 */
public class SuffixSortExample {
    
    private static String longestRepeatedSubString(String input) {
        //to be or not to be ---> to be
        
        return null;
    }
    
    
    private static String longestRepeatedSubStringSuffix(List<String> l) {
        //use suffix array 
        //a repeated prefix 
        String currLCP=null;
        int currLCPLen=0;
        for (int i=0;i<l.size()-1;i++) {
            //System.out.println(l.get(i)+" "+l.get(i+1));
            String currLCP_ = lcp2Strings(l.get(i),l.get(i+1));
            if(currLCPLen<currLCP_.length()) {
                currLCP=currLCP_;
                currLCPLen = currLCP_.length();
            }
        }
        //System.out.println(currLCPLen +" "+currLCP);
        return currLCP;
    }
    
    private static String lcp2Strings(String s1, String s2) {
        if(s1==null || s2==null) return null;
        if(s1.length()==0 || s2.length()==0) return null;
        int maxPrefixPossible = Math.min(s1.length(), s2.length());
        int count=0;
        while(s1.charAt(count)==s2.charAt(count)) {
            count++;
            if(count==maxPrefixPossible) break;
        }
        return s1.substring(0, count);
        
    } //lcp2Strings
    
    private static List<String> suffixArray(String input) {
        /*
        abc  -> abc bc c
        */
        List<String> suffixList = new ArrayList<String>();
        int len = input.length();
        for (int i=0; i<len;i ++) {
            suffixList.add(input.substring(i, len));
        }
        Collections.sort(suffixList);
        return suffixList;
    }
    
    private static void printSuffixArray(List<String> l) {
        for(String s:l) {
            System.out.println(s+" ");
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        
        
        //lcp2strings
        String s1="accgttaa";
        String s2 = "accx";
        //System.out.println(lcp2Strings(s1,s2));
        
        //test suffixArray
        String s = "aacaagtttacaagc";
        List<String> l = suffixArray(s);
        //printSuffixArray(l);
        String lrs = longestRepeatedSubStringSuffix(l); //longestRepeatedSubstring
        System.out.println("longestRepeatedSubStringSuffix of " +s+" is "+lrs);
        
        String input = "hello";
        SuffixArray sArr = new SuffixArray(input);
        
    }//main
}
