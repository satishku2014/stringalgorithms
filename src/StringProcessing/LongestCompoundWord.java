/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package StringProcessing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * Using helper code from http://algs4.cs.princeton.edu/code/
 * 
 * Problem: Find out the longest string which is composed of other strings in the dictionary
 * 
 * @author sverma
 */


public class LongestCompoundWord {
    
    public static void main(String[] args) {
        
        TrieST stringTrie = new TrieST();
        Queue stringQueue = new Queue();
        
        String[] words = {"cat","cats","catsdogcats","catxdogcatsrat","dog","dogcatsdog","hippopotamuses", "rat", "ratcat", "ratcatdog", "ratcatdogcat" };
        
        //populate the trie
        for (String word : words) {
            //System.out.println(word);
            stringTrie.put(word, 1);
        }//for
        
        //populate the queue 
        String prefixOF;
        for (String word : words) {
            Iterable<String> it = stringTrie.keysWithPrefix(word);
            System.out.print(word+" : ");
            Iterator iter = it.iterator();
            while (iter.hasNext()) {
                prefixOF = iter.next().toString();
                if(!word.equals(prefixOF)) {
                   //System.out.println(word+" "+prefixOF + " ");
                   //populate the queue
                   String suff = suffix(word,prefixOF); //word is a prefix of prefixOF
                   List<String> pair=new ArrayList<String>();
                   pair.add(prefixOF);
                   pair.add(suff);
                   stringQueue.enqueue(pair);
                }
            }
           //System.out.println();
        } //for
        
        
        //System.out.println(stringQueue.size());
        
        /*
         * Previous Step Populates the QUEUE 
         * 
         */
        
        
        
       String longestWORD = "";
       System.out.println(stringQueue.toString());
        while(!stringQueue.isEmpty()) {
            List<String> pair = (List<String>)stringQueue.dequeue();
            String first = pair.get(0);
            String second = pair.get(1);
            //System.out.println(first +" "+first.length()+" "+second+" "+second.length());
            if(second.length()==0){
                //System.out.println("possible solution "+first +" "+first.length());
                if(first.length()>longestWORD.length()) longestWORD=first;
            } else {
                //find all prefix of the second word
                //System.out.println("suffix is "+second);
                //get all prefixes for second 
                List<String> prefList = new ArrayList<String>();
                
                for(String w:words) {
                    if(second.startsWith(w)) {
                        String suff_ = suffix(w,second);
                          List<String> pair_=new ArrayList<String>();
                          pair_.add(first);
                          pair_.add(suff_);stringQueue.enqueue(pair_);
                    }
                    
                }
                
            }
            
            //System.out.println(stringQueue.toString());
        }
        
        
        System.out.println("Answer " + longestWORD + " " +longestWORD.length());
        
    } //main
    
    
    
    
    
    
    private static String suffix(String word,String bigWord) {
                String suff;
                suff= bigWord.substring(word.length(), bigWord.length());
                return suff;          
    }
    
    
}//LongestCompoundWord
